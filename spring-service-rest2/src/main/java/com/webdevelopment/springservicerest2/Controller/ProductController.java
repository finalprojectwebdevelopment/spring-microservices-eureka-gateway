package com.webdevelopment.springservicerest2.Controller;

import com.webdevelopment.springservicerest2.Model.Product;
import com.webdevelopment.springservicerest2.Repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/product")
public class ProductController {
    
    @Autowired
    private ProductRepository repo;

    @GetMapping("/Get")
    public Iterable<Product> getProductList(){
        return repo.findAll();
    }


}
