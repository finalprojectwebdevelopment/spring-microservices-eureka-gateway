package com.webdevelopment.springservicerest2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMethod;

@SpringBootApplication
@EnableEurekaClient
@CrossOrigin(origins = "*", methods={RequestMethod.POST,RequestMethod.PUT})
public class SpringServiceRest2Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringServiceRest2Application.class, args);
    }

}
