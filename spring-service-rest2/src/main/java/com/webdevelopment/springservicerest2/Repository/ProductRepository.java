package com.webdevelopment.springservicerest2.Repository;

import com.webdevelopment.springservicerest2.Model.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, String> {
    
}
