    ### Spring --> (Dicovery (Eureka), Gateway Cloud, Microservice).

    This project contains:
    + cloud-gateway (Spring Cloud Gateway) --> port: 9090, Name: Gateway-Cloud.
    + service-register (Spring Eureka Discovery)--> port: 8585, Without name.
    + spring-service-rest1 --> port: 8180, Name: springrest1.
    + spring-service-rest1-1 --> port: 8181, Name: springrest1. ***
    + spring-service-rest2 --> port: 8280, Name: springrest2.
    + spring-service-rest2-1 --> port: 8281, Name: springrest2. ***
    Not here **:
    + Express-Service-1 --> port: 3010
    + Express-Service-1.1 --> port: 3011 ***
    + Express-Service-2 --> port: 3020
    + Express-Service-2.1 --> port: 3021 ***

   ** This project discover services with nodejs.
   ** To use all project download the 4 services with node.
      Find here:

    *** Services:
                                <-- Spring -->
        --> spring-service-rest1-1 is equal to spring-service-rest1 (To use lb --> Load Balancer)
        --> spring-service-rest2-1 is equal to spring-service-rest2 (To use lb --> Load Balancer)

                                 <-- Nodejs -->
        --> nodejsrest1.1 is equal to nodejsrest1 (To use lb --> Load Balancer)
        --> nodejsrest2.1 is equal to nodejsrest2 (To use lb --> Load Balancer)