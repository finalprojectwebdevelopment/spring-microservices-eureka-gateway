package com.webdevelopment.springservicerest11.Repository;

import org.springframework.data.repository.CrudRepository;
import com.webdevelopment.springservicerest11.Model.Product;

public interface ProductRepository extends CrudRepository<Product, String> {
    
}
