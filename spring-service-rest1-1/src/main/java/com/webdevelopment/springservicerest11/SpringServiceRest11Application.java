package com.webdevelopment.springservicerest11;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMethod;

@EnableEurekaClient
@SpringBootApplication
@CrossOrigin(origins = "*", methods={RequestMethod.POST,RequestMethod.PUT})
public class SpringServiceRest11Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringServiceRest11Application.class, args);
    }

}
