package com.webdevelopment.springservicerest21.Repository;

import com.webdevelopment.springservicerest21.Model.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, String> {
    
}
