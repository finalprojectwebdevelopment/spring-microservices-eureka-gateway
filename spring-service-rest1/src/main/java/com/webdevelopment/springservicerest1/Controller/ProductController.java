package com.webdevelopment.springservicerest1.Controller;

import com.webdevelopment.springservicerest1.Repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/product")
public class ProductController {
    
    @Autowired
    private ProductRepository repo;

    @DeleteMapping("/{id}")
	public void deleteItem(@PathVariable String id) {
		repo.deleteById(id);
	}
}
