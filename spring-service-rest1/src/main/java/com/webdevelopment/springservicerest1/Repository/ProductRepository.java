package com.webdevelopment.springservicerest1.Repository;

import com.webdevelopment.springservicerest1.Model.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, String> {
    
}
